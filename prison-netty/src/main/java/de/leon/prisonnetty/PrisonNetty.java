package de.leon.prisonnetty;

import de.leon.commons.netty.NettyBootstrap;

public class PrisonNetty {

    public static void main(String[] args) {
        while (true) {
            System.out.println("========================| Booting Netty Server |========================");
            NettyBootstrap.setupServerBootstrap(8888);
            System.out.println("========================| Restarting Netty Server |========================");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
