package de.leon.prisonspigot.listener;

import de.leon.commons.netty.ClientBootstrap;
import de.leon.commons.netty.packet.packets.UpdateUserPacket;
import de.leon.commons.user.User;
import de.leon.prisonspigot.Prison;
import de.leon.prisonspigot.util.Inmate;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.Locale;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();
        User user = Prison.getInstance().getUserHandler().get(p.getUniqueId());
        Inmate inmate = Prison.getInstance().getInmateHandler().get(p.getUniqueId());

        if (e.getClickedInventory() == null || e.getClickedInventory() != p.getOpenInventory().getTopInventory()) return;

        if (e.getClickedInventory().getTitle().equals(user.translate("language-gui-name"))) {
            if (e.getCurrentItem() == null) return;

            e.setCancelled(true);

            Locale locale = null;
            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(user.translate("language-name-german"))) {
                locale = Locale.GERMANY;
            } else if (e.getCurrentItem().getItemMeta().getDisplayName().equals(user.translate("language-name-english"))) {
                locale = Locale.US;
            }
            if (locale != null) {

                user.setLanguage(Prison.getInstance().getLanguageHandler().get(locale.toString()));
                Prison.getInstance().getUserHandler().updateAsynchronously(Prison.getInstance().getExecutorService(), p.getUniqueId(), user);
                ClientBootstrap.send(new UpdateUserPacket(p.getUniqueId()));

                p.sendMessage(user.translate("language-changed", user.translate("language-name")));
                p.closeInventory();

                inmate.updateTablist(p, user);
                inmate.createScoreboard(p, user);

            }
        }
    }
}
