package de.leon.prisonspigot.listener;

import de.leon.prisonspigot.Prison;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player p = e.getPlayer();
        e.setQuitMessage("");

        AsyncPlayerPreLoginListener.getFutureMap().remove(p.getUniqueId());
        Prison.getInstance().getUserHandler().remove(p.getUniqueId());
        Prison.getInstance().getInmateHandler().remove(p.getUniqueId());

    }
}
