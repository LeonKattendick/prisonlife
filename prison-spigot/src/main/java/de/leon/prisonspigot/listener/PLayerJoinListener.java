package de.leon.prisonspigot.listener;

import de.leon.commons.user.User;
import de.leon.prisonspigot.Prison;
import de.leon.prisonspigot.util.Inmate;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PLayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();
        e.setJoinMessage("");

        Prison.getInstance().getExecutorService().execute(() -> {
            try {

                User user = AsyncPlayerPreLoginListener.getFutureMap().get(p.getUniqueId()).get(15, TimeUnit.SECONDS);
                Inmate inmate = Prison.getInstance().getInmateHandler().get(p.getUniqueId());

                inmate.updateTablist(p, user);
                inmate.createScoreboard(p, user);

                if (!user.isNewUser()) return;

                p.sendMessage(user.translate("help-message"));

            } catch (InterruptedException | ExecutionException | TimeoutException ex) {
                ex.printStackTrace();
            }
        });
    }
}
