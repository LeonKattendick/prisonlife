package de.leon.prisonspigot.listener;

import com.google.common.util.concurrent.SettableFuture;
import de.leon.commons.user.User;
import de.leon.prisonspigot.Prison;
import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AsyncPlayerPreLoginListener implements Listener {

    @Getter
    private static final Map<UUID, SettableFuture<User>> futureMap = new HashMap<>();

    @EventHandler
    public void onPreLogin(AsyncPlayerPreLoginEvent e) {

        getFutureMap().put(e.getUniqueId(), SettableFuture.create());

        User user = Prison.getInstance().getUserHandler().load(e.getUniqueId());
        if (user != null) getFutureMap().get(e.getUniqueId()).set(user);

        if (!Prison.getInstance().getInmateHandler().isRegistered(e.getUniqueId())) {
            Prison.getInstance().getInmateHandler().register(e.getUniqueId());
        }
        Prison.getInstance().getInmateHandler().load(e.getUniqueId());
    }
}
