package de.leon.prisonspigot.commands;

import de.leon.commons.user.User;
import de.leon.prisonspigot.Prison;
import de.leon.prisonspigot.util.InventoryHelper;
import de.leon.prisonspigot.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class LanguageCommand implements CommandExecutor {

    private static final String GERMAN_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWU3ODk5YjQ4MDY4NTg2OTdlMjgzZjA4NGQ5MTczZmU0ODc4ODY0NTM3NzQ2MjZiMjRiZDhjZmVjYzc3YjNmIn19fQ==";

    private static final String ENGLISH_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGNhYzk3NzRkYTEyMTcyNDg1MzJjZTE0N2Y3ODMxZjY3YTEyZmRjY2ExY2YwY2I0YjM4NDhkZTZiYzk0YjQifX19";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {

        Player p = (Player) sender;
        User user = Prison.getInstance().getUserHandler().get(p.getUniqueId());

        Inventory inventory = Bukkit.createInventory(null, 9, user.translate("language-gui-name"));
        InventoryHelper.fill(inventory, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, 15).setDisplayName(" ").build());

        inventory.setItem(3, new ItemBuilder(Material.SKULL_ITEM, 1, 3).setTexture(ENGLISH_TEXTURE).setDisplayName(user.translate("language-name-english")).build());
        inventory.setItem(5, new ItemBuilder(Material.SKULL_ITEM, 1, 3).setTexture(GERMAN_TEXTURE).setDisplayName(user.translate("language-name-german")).build());

        p.openInventory(inventory);

        return true;
    }
}
