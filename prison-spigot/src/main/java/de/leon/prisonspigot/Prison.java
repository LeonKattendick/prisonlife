package de.leon.prisonspigot;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.leon.commons.language.LanguageHandler;
import de.leon.commons.netty.ClientBootstrap;
import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.NettyBootstrap;
import de.leon.commons.netty.packet.listener.UpdateUserPacketListener;
import de.leon.commons.netty.packet.packets.UpdateUserPacket;
import de.leon.commons.sql.MySQL;
import de.leon.commons.user.User;
import de.leon.commons.user.UserHandler;
import de.leon.prisonspigot.commands.LanguageCommand;
import de.leon.prisonspigot.listener.AsyncPlayerPreLoginListener;
import de.leon.prisonspigot.listener.InventoryClickListener;
import de.leon.prisonspigot.listener.PLayerJoinListener;
import de.leon.prisonspigot.listener.PlayerQuitListener;
import de.leon.prisonspigot.util.Inmate;
import de.leon.prisonspigot.util.InmateHandler;
import de.leon.prisonspigot.util.callbacks.UpdateUserCallback;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Executors;

@Getter
public class Prison extends JavaPlugin {

    @Getter
    private static Prison instance;

    private final ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

    private MySQL mySQL;

    private LanguageHandler languageHandler;

    private UserHandler userHandler;

    private InmateHandler inmateHandler;

    @Override
    public void onEnable() {

        instance = this;

        this.mySQL = new MySQL(getDataFolder());

        NettyBootstrap.setupClientBootstrap("localhost", 8888, getExecutorService(), new DistributionType[] {DistributionType.SPIGOT});

        this.languageHandler = new LanguageHandler();
        getLanguageHandler().init(getDataFolder());

        this.userHandler = new UserHandler(getMySQL(), getLanguageHandler());
        getUserHandler().initTables();

        this.inmateHandler = new InmateHandler(getMySQL());
        getInmateHandler().initTables();

        this.getCommand("language").setExecutor(new LanguageCommand());

        Bukkit.getPluginManager().registerEvents(new AsyncPlayerPreLoginListener(), this);
        Bukkit.getPluginManager().registerEvents(new PLayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);

        NettyBootstrap.registerListener(new UpdateUserPacketListener(new UpdateUserCallback(), getExecutorService(), getUserHandler()), UpdateUserPacket.class);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> Bukkit.getOnlinePlayers().forEach(entry -> {

            User user = getUserHandler().get(entry.getUniqueId());
            Inmate inmate = getInmateHandler().get(entry.getUniqueId());

            if (user != null && inmate != null) inmate.updateScoreboardDisplayName(entry, user);

        }), 0, 20 * 10);
    }

    @Override
    public void onDisable() {
        getMySQL().close();
        ClientBootstrap.getClientBootstrap().getChannelHandler().close();
    }
}
