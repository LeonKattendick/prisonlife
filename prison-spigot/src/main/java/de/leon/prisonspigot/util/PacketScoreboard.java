package de.leon.prisonspigot.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class PacketScoreboard {

    @NonNull
    private Player player;

    private final Map<Integer, String> lines = new HashMap<>();

    public void createScoreboard(String name) {

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_OBJECTIVE);
        container.getStrings().write(0, "sidebar").write(1, name);
        container.getIntegers().write(0, 0);
        container.getEnumModifier(DisplayType.class, 2).write(0, DisplayType.INTEGER);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void updateDisplayName(String name) {

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_OBJECTIVE);
        container.getStrings().write(0, "sidebar").write(1, name);
        container.getIntegers().write(0, 2);
        container.getEnumModifier(DisplayType.class, 2).write(0, DisplayType.INTEGER);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void destroyScoreboard() {

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_OBJECTIVE);
        container.getStrings().write(0, "sidebar");
        container.getIntegers().write(0, 1);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void showScoreboard() {

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE);
        container.getIntegers().write(0, 1);
        container.getStrings().write(0, "sidebar");

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void setLine(int line, String content) {
        if (getLines().containsKey(line)) removeLine(line);

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_SCORE);
        container.getStrings().write(0, content).write(1, "sidebar");
        container.getScoreboardActions().write(0, EnumWrappers.ScoreboardAction.CHANGE);
        container.getIntegers().write(0, line);

        getLines().put(line, content);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void removeLine(int line) {
        if (!getLines().containsKey(line)) return;

        PacketContainer container = new PacketContainer(PacketType.Play.Server.SCOREBOARD_SCORE);
        container.getStrings().write(0, getLines().get(line)).write(1, "sidebar");
        container.getScoreboardActions().write(0, EnumWrappers.ScoreboardAction.REMOVE);

        getLines().remove(line);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(getPlayer(), container);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private enum DisplayType {
        INTEGER;
    }
}
