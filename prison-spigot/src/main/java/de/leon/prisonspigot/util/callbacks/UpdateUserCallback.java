package de.leon.prisonspigot.util.callbacks;

import de.leon.commons.user.User;
import de.leon.commons.util.Callback;
import de.leon.commons.util.Pair;
import de.leon.prisonspigot.Prison;
import de.leon.prisonspigot.listener.AsyncPlayerPreLoginListener;
import de.leon.prisonspigot.util.Inmate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class UpdateUserCallback implements Callback<Pair<UUID, User>> {

    @Override
    public void invoke(Pair<UUID, User> pair) {

        Player p = Bukkit.getPlayer(pair.getA());
        if (p == null) return;

        User user = Prison.getInstance().getUserHandler().load(pair.getA());
        if (AsyncPlayerPreLoginListener.getFutureMap().containsKey(pair.getA()) && user != null) AsyncPlayerPreLoginListener.getFutureMap().get(pair.getA()).set(user);

        Inmate inmate = Prison.getInstance().getInmateHandler().get(pair.getA());
        if (user == null || inmate == null) return;

        inmate.updateTablist(p, user);
        inmate.createScoreboard(p, user);

    }
}
