package de.leon.prisonspigot.util;

import de.leon.commons.sql.MySQL;
import de.leon.commons.sql.PlayerBasedSQLHandler;
import de.leon.commons.sql.result.Result;
import de.leon.commons.sql.result.Row;

import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

public class InmateHandler extends PlayerBasedSQLHandler<Inmate> {

    public static final CellblockType DEFAULT = CellblockType.A;

    public InmateHandler(MySQL mySQL) {
        super(mySQL, new HashMap<>());
    }

    @Override
    public void initTables() {
        getMySQL().update("CREATE TABLE IF NOT EXISTS `inmates` (uuid VARCHAR(36) PRIMARY KEY, cellblock VARCHAR(4), prestige INT, money BIGINT, gems INT)");
    }

    @Override
    public boolean isRegistered(UUID uuid) {

        Result result = getMySQL().query("SELECT * FROM `inmates` WHERE uuid = ? LIMIT 1", uuid.toString());

        return result.iterator().hasNext();
    }

    @Override
    public void register(UUID uuid) {
        getMySQL().update("INSERT INTO `inmates` (uuid, cellblock, prestige, money, gems) VALUES (?, ?, 0, 0, 0)", uuid.toString(), DEFAULT.toString());
    }

    @Override
    public Inmate load(UUID uuid) {

        Inmate inmate = null;
        Result result = getMySQL().query("SELECT * FROM `inmates` WHERE uuid = ? LIMIT 1", uuid.toString());
        Iterator<Row> iterator = result.iterator();

        if (iterator.hasNext()) {
            Row row = iterator.next();
            getMap().put(uuid, inmate = new Inmate(CellblockType.valueOf(row.getString("cellblock")), row.getInt("prestige"), row.getLong("money"), row.getInt("gems")));
        }
        return inmate;
    }

    @Override
    public Inmate update(UUID uuid, Inmate inmate) {
        getMySQL().update("UPDATE `inmates` SET cellblock = ?, prestige = ?, money = ?, gems = ? WHERE uuid = ?", inmate.getCellblock().toString(), inmate.getPrestige(), inmate.getMoney(), inmate.getGems(), uuid.toString());
        return inmate;
    }
}
