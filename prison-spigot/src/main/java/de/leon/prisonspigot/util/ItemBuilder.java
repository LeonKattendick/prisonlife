package de.leon.prisonspigot.util;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.block.Skull;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.UUID;

@Getter
public class ItemBuilder {

    private ItemStack itemStack;

    private ItemMeta itemMeta;

    public ItemBuilder(Material material) {
        this.itemStack = new ItemStack(material);
        this.itemMeta = getItemStack().getItemMeta();
    }

    public ItemBuilder(Material material, int amount) {
        this.itemStack = new ItemStack(material, amount);
        this.itemMeta = getItemStack().getItemMeta();
    }

    public ItemBuilder(Material material, int amount, int durability) {
        this.itemStack = new ItemStack(material, amount, (short) durability);
        this.itemMeta = getItemStack().getItemMeta();
    }

    public ItemBuilder setDisplayName(String name) {
        getItemMeta().setDisplayName(name);
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        getItemMeta().setLore(Arrays.asList(lore));
        return this;
    }

    public ItemBuilder setOwner(String owner) {
        ((SkullMeta) getItemMeta()).setOwner(owner);
        return this;
    }

    public ItemBuilder setTexture(String texture) {

        GameProfile gameProfile = new GameProfile(UUID.randomUUID(), null);
        gameProfile.getProperties().put("textures", new Property("textures", texture));

        try {
            Field field = getItemMeta().getClass().getDeclaredField("profile");
            field.setAccessible(true);
            field.set(getItemMeta(), gameProfile);
        } catch (IllegalArgumentException | NoSuchFieldException | SecurityException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;
    }

    public ItemStack build() {
        getItemStack().setItemMeta(getItemMeta());
        return getItemStack();
    }
}
