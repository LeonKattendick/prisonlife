package de.leon.prisonspigot.util;

import de.leon.commons.user.User;
import de.leon.commons.util.RomanNumerals;
import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

@Getter
@Setter
@RequiredArgsConstructor
public class Inmate {

    @NonNull
    private CellblockType cellblock;

    @NonNull
    private int prestige;

    @NonNull
    private long money;

    @NonNull
    private int gems;

    private PacketScoreboard scoreboard;

    public void updateTablist(Player p, User user) {

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getMainScoreboard();
        String name = getPrestige() + user.getType().getScoreboardTeamName();

        Team team = scoreboard.getTeam(name);
        if (team == null) team = scoreboard.registerNewTeam(name);

        team.setPrefix((getPrestige() > 0 ? "§b[" + RomanNumerals.intToNumeral(getPrestige()) + "] " : "") + user.getType().getTablist());
        team.addEntry(p.getName());

        p.setScoreboard(scoreboard);

    }

    public void createScoreboard(Player p, User user) {
        if (getScoreboard() != null) {
            getScoreboard().destroyScoreboard();
        }

        this.scoreboard = new PacketScoreboard(p);

        getScoreboard().createScoreboard(user.translate("scoreboard-title", Bukkit.getOnlinePlayers().size(), Bukkit.getMaxPlayers()));
        getScoreboard().showScoreboard();

        getScoreboard().setLine(22, user.translate("scoreboard-line-0"));
        getScoreboard().setLine(21, user.translate("scoreboard-line-1"));
        getScoreboard().setLine(20, user.translate("scoreboard-line-2", getCellblock().toString()));
        getScoreboard().setLine(19, user.translate("scoreboard-line-3", getCellblock().toString()));
        getScoreboard().setLine(18, user.translate("scoreboard-line-4"));
        getScoreboard().setLine(17, user.translate("scoreboard-line-5", RomanNumerals.intToNumeral(getPrestige())));
        getScoreboard().setLine(16, user.translate("scoreboard-line-6"));
        getScoreboard().setLine(15, user.translate("scoreboard-line-7", getMoney()));
        getScoreboard().setLine(14, user.translate("scoreboard-line-8", 0));
        getScoreboard().setLine(13, user.translate("scoreboard-line-9", getGems()));
        getScoreboard().setLine(12, user.translate("scoreboard-line-10"));
        getScoreboard().setLine(11, user.translate("scoreboard-line-11"));
        getScoreboard().setLine(10, user.translate("scoreboard-line-12"));

    }

    public void updateScoreboard(Player p, User user) {
        getScoreboard().setLine(20, user.translate("scoreboard-line-2", getCellblock().toString()));
        getScoreboard().setLine(19, user.translate("scoreboard-line-3", getCellblock().toString()));
        getScoreboard().setLine(17, user.translate("scoreboard-line-5", RomanNumerals.intToNumeral(getPrestige())));
        getScoreboard().setLine(15, user.translate("scoreboard-line-7", getMoney()));
        getScoreboard().setLine(14, user.translate("scoreboard-line-8", 0));
        getScoreboard().setLine(13, user.translate("scoreboard-line-9", getGems()));
    }

    public void updateScoreboardDisplayName(Player p, User user) {
        getScoreboard().updateDisplayName(user.translate("scoreboard-title", Bukkit.getOnlinePlayers().size(), Bukkit.getMaxPlayers()));
    }
}
