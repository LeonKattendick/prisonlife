package de.leon.prisonspigot.util;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryHelper {

    public static void fill(Inventory inventory, ItemStack itemStack) {
        for (int i = 0; i < inventory.getSize(); i++) inventory.setItem(i, itemStack);
    }
}
