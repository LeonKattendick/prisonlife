package de.leon.prisondiscord.util;

import lombok.Getter;
import net.cubespace.Yamler.Config.YamlConfig;

import java.io.File;

@Getter
public class ConfigFile extends YamlConfig {

    private String token = "-", guild = "-";

    public ConfigFile() {
        CONFIG_FILE = new File("./configs/", "config.yml");
    }
}
