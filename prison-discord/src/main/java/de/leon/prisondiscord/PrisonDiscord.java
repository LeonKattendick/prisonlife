package de.leon.prisondiscord;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.NettyBootstrap;
import de.leon.commons.sql.MySQL;
import de.leon.prisondiscord.util.ConfigFile;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.util.concurrent.Executors;

@Getter
public class PrisonDiscord {

    private final ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

    private ConfigFile configFile;

    private final JDA jda;

    private final Guild guild;

    private final MySQL mySQL;

    public PrisonDiscord() throws LoginException, InterruptedException {

        try {
            configFile = new ConfigFile();
            getConfigFile().init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        jda = JDABuilder.createDefault(getConfigFile().getToken()).setActivity(Activity.playing("PrisonLife.EU")).build();
        getJda().awaitReady();

        guild = getJda().getGuildById(getConfigFile().getGuild());

        mySQL = new MySQL(new File("./configs/"));

        NettyBootstrap.setupClientBootstrap("localhost", 8888, getExecutorService(), new DistributionType[] {DistributionType.DISCORD});

    }

    public static void main(String[] args) {
        try {
            new PrisonDiscord();
        } catch (LoginException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
