package de.leon.prisonbungee;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.leon.commons.language.LanguageHandler;
import de.leon.commons.netty.ClientBootstrap;
import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.NettyBootstrap;
import de.leon.commons.netty.packet.packets.UpdateUserPacket;
import de.leon.commons.sql.MySQL;
import de.leon.commons.netty.packet.listener.UpdateUserPacketListener;
import de.leon.commons.user.UserHandler;
import de.leon.prisonbungee.listener.LoginListener;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.Executors;

@Getter
public class Prison extends Plugin {

    @Getter
    private static Prison instance;

    private final ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

    private MySQL mySQL;

    private LanguageHandler languageHandler;

    private UserHandler userHandler;

    @Override
    public void onEnable() {

        instance = this;

        mySQL = new MySQL(getDataFolder());

        NettyBootstrap.setupClientBootstrap("localhost", 8888, getExecutorService(), new DistributionType[] {DistributionType.BUNGEE});

        languageHandler = new LanguageHandler();
        getLanguageHandler().init(getDataFolder());

        userHandler = new UserHandler(getMySQL(), getLanguageHandler());
        getUserHandler().initTables();

        ProxyServer.getInstance().getPluginManager().registerListener(this, new LoginListener());

        NettyBootstrap.registerListener(new UpdateUserPacketListener(null, getExecutorService(), getUserHandler()), UpdateUserPacket.class);

    }

    @Override
    public void onDisable() {
        getMySQL().close();
        ClientBootstrap.getClientBootstrap().getChannelHandler().close();
    }
}
