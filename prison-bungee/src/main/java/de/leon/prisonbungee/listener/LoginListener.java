package de.leon.prisonbungee.listener;

import de.leon.commons.netty.ClientBootstrap;
import de.leon.commons.netty.packet.packets.UpdateUserPacket;
import de.leon.prisonbungee.Prison;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class LoginListener implements Listener {

    @EventHandler
    public void onPreLogin(LoginEvent e) {
        if (!Prison.getInstance().getUserHandler().isRegistered(e.getConnection().getUniqueId())) {
            Prison.getInstance().getUserHandler().register(e.getConnection().getUniqueId());
            ClientBootstrap.send(new UpdateUserPacket(e.getConnection().getUniqueId()));
        }
        Prison.getInstance().getUserHandler().load(e.getConnection().getUniqueId());
    }
}
