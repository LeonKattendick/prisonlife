package de.leon.commons.netty.packet.packets;

import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

import java.util.UUID;

@Getter
public class UpdateUserPacket extends Packet {

    private UUID uuid;

    public UpdateUserPacket() {
        super(new DistributionType[] {DistributionType.SPIGOT, DistributionType.BUNGEE});
    }

    public UpdateUserPacket(UUID uuid) {
        this();
        this.uuid = uuid;
    }

    @Override
    public void write(ByteBuf byteBuf) {
        writeUUID(getUuid(), byteBuf);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.uuid = readUUID(byteBuf);
    }
}
