package de.leon.commons.netty;

import com.google.common.util.concurrent.ListeningExecutorService;
import de.leon.commons.netty.handler.ClientChannelHandler;
import de.leon.commons.netty.packet.Packet;
import de.leon.commons.netty.packet.packets.DistributionPacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ClientBootstrap implements Runnable {

    @Getter
    public static ClientBootstrap clientBootstrap;

    @NonNull
    private final String host;

    @NonNull
    private final int port;

    @NonNull
    private final ListeningExecutorService executorService;

    @NonNull
    private final ClientChannelHandler channelHandler;

    @NonNull
    private final DistributionType[] distributionTypes;

    @Override
    public void run() {

        EventLoopGroup group = new NioEventLoopGroup();

        try {

            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group).channel(NioSocketChannel.class).handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) {
                    NettyBootstrap.preparePipeline(socketChannel, getChannelHandler());
                }
            }).option(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.connect(getHost(), getPort()).channel().closeFuture().addListener(future -> {

                System.out.println("[PROTOCOL] Netty connection lost. Try to reconnect in 5 seconds...");

                Thread.sleep(5000);
                startClient(getHost(), getPort(), getExecutorService(), new ClientChannelHandler(), getDistributionTypes());

                group.shutdownGracefully();

            }).sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }

    public static void send(Packet packet) {
        getClientBootstrap().getChannelHandler().send(packet);
    }

    protected static void startClient(String host, int port, ListeningExecutorService executorService, ClientChannelHandler channelHandler, DistributionType[] distributionTypes) {
        clientBootstrap = new ClientBootstrap(host, port, executorService, channelHandler, distributionTypes);
        executorService.execute(getClientBootstrap());
        channelHandler.send(new DistributionPacket(distributionTypes));
    }
}
