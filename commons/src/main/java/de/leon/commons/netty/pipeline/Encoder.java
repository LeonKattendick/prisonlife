package de.leon.commons.netty.pipeline;

import de.leon.commons.netty.Protocol;
import de.leon.commons.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class Encoder extends MessageToByteEncoder<Packet> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Packet packet, ByteBuf byteBuf) {

        int packetId = Protocol.getIdByClass(packet.getClass());
        if (packetId == 0) return;

        byteBuf.writeInt(packetId);
        packet.write(byteBuf);

    }
}
