package de.leon.commons.netty;

public enum DistributionType {
    ALL, SPIGOT, BUNGEE, DISCORD, NETTY;
}
