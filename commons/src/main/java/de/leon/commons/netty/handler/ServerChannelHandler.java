package de.leon.commons.netty.handler;

import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.Protocol;
import de.leon.commons.netty.packet.Packet;
import de.leon.commons.netty.packet.packets.DistributionPacket;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Getter;

import java.util.*;

public class ServerChannelHandler extends SimpleChannelInboundHandler<Packet> {

    @Getter
    private static final HashMap<Channel, List<DistributionType>> channels = new HashMap<>();

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) {
        System.out.println("[REGISTER] Hash: " + ctx.channel().hashCode());
        getChannels().put(ctx.channel(), Collections.singletonList(DistributionType.ALL));
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) {
        System.out.println("[UNREGISTER] Hash: " + ctx.channel().hashCode());
        getChannels().remove(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) {
        if (packet instanceof DistributionPacket) {

            DistributionPacket distributionPacket = (DistributionPacket) packet;
            getChannels().put(channelHandlerContext.channel(), Arrays.asList(distributionPacket.getDistributionTypes()));

            System.out.println("[DISTRIBUTION] Hash: " + channelHandlerContext.channel().hashCode() + ", Distributions: " + Arrays.toString(distributionPacket.getDistributionTypes()));

        } else {

            int id = Protocol.getIdByClass(packet.getClass());
            List<DistributionType> distributionTypes = Arrays.asList(packet.getDistributionTypes());

            getChannels().entrySet().stream().filter(entry -> entry.getKey().hashCode() != channelHandlerContext.channel().hashCode() && (distributionTypes.contains(DistributionType.ALL) || getChannels().get(entry.getKey()).stream().anyMatch(distributionTypes::contains))).forEach(entry -> entry.getKey().writeAndFlush(packet));
            System.out.println("[ECHO] PacketId: " + id + ", Class: " + packet.getClass().getSimpleName() + ", Distributed: " + distributionTypes);

        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }
}
