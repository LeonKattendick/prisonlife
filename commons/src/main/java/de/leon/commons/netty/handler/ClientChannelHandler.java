package de.leon.commons.netty.handler;

import de.leon.commons.netty.ClientBootstrap;
import de.leon.commons.netty.NettyBootstrap;
import de.leon.commons.netty.Protocol;
import de.leon.commons.netty.packet.Packet;
import de.leon.commons.netty.packet.PacketListener;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Getter
@NoArgsConstructor
public class ClientChannelHandler extends SimpleChannelInboundHandler<Packet> {

    private final BlockingQueue<Packet> sendBefore = new LinkedBlockingQueue<>();

    private Channel channel;

    public void send(Packet packet) {
        if (this.channel == null) getSendBefore().add(packet);
        else getChannel().writeAndFlush(packet);
    }

    public void close() {
        if (getChannel() != null) getChannel().close();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        channel = ctx.channel();
        getSendBefore().forEach(entry -> getChannel().writeAndFlush(entry));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Packet packet) {
        NettyBootstrap.getListeners().entrySet().stream().filter(entry -> packet.getClass().equals(entry.getKey())).forEach(entry -> entry.getValue().forEach(item -> item.onReceive(Protocol.cast(packet, entry.getKey()))));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }
}
