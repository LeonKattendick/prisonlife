package de.leon.commons.netty;

import com.google.common.reflect.ClassPath;
import de.leon.commons.netty.packet.Packet;
import lombok.Getter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Protocol {

    @Getter
    private static final Map<Integer, Class<? extends Packet>> packets = new HashMap<>();

    public static synchronized void init() {
        try {
            ClassPath.from(Protocol.class.getClassLoader()).getTopLevelClassesRecursive("de.leon.commons.netty.packet.packets").forEach(entry -> registerPacket(entry.getSimpleName(), (Class<? extends Packet>) entry.load()));
            System.out.println("[PROTOCOL] Loaded " + getPackets().size() + " different packets.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void registerPacket(String name, Class<? extends Packet> packetClass) {
        getPackets().put(name.hashCode(), packetClass);
    }

    public static Class<? extends Packet> getClassById(int id) {
        return getPackets().get(id);
    }

    public static int getIdByClass(Class<? extends Packet> packetClass) {
        return getPackets().entrySet().stream().filter(entry -> entry.getValue().equals(packetClass)).map(Map.Entry::getKey).findFirst().orElse(0);
    }

    public static <T> T cast(Packet packet, Class<? extends Packet> packetClass) {
        try {
            return (T) packetClass.cast(packet);
        } catch (ClassCastException e) {
        }
        return null;
    }
}
