package de.leon.commons.netty.packet.packets;

import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

@Getter
public class StringPacket extends Packet {

    private String text;

    public StringPacket() {
        super(new DistributionType[] {DistributionType.ALL});
    }

    public StringPacket(String text) {
        this();
        this.text = text;
    }

    @Override
    public void write(ByteBuf byteBuf) {
        writeString(getText(), byteBuf);
    }

    @Override
    public void read(ByteBuf byteBuf) {
        this.text = readString(byteBuf);
    }
}
