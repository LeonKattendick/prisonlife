package de.leon.commons.netty.packet.packets;

import de.leon.commons.netty.DistributionType;
import de.leon.commons.netty.packet.Packet;
import io.netty.buffer.ByteBuf;
import lombok.Getter;

@Getter

public class DistributionPacket extends Packet {

    private DistributionType[] distributionTypes;

    public DistributionPacket() {
        super(new DistributionType[] {DistributionType.NETTY});
    }

    public DistributionPacket(DistributionType[] distributionTypes) {
        this();
        this.distributionTypes = distributionTypes;
    }

    @Override
    public void write(ByteBuf byteBuf) {

        byteBuf.writeInt(getDistributionTypes().length);
        for (DistributionType entry : getDistributionTypes()) writeEnum(entry, byteBuf);

    }

    @Override
    public void read(ByteBuf byteBuf) {

        DistributionType[] distributionTypes = new DistributionType[byteBuf.readInt()];
        for (int i = 0; i < distributionTypes.length; i++) distributionTypes[i] = readEnum(byteBuf, DistributionType.values());
        this.distributionTypes = distributionTypes;

    }
}
