package de.leon.commons.netty.packet.listener;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListeningExecutorService;
import de.leon.commons.util.Callback;
import de.leon.commons.util.FutureCallbackAdapter;
import de.leon.commons.netty.packet.PacketListener;
import de.leon.commons.netty.packet.packets.UpdateUserPacket;
import de.leon.commons.user.User;
import de.leon.commons.user.UserHandler;
import de.leon.commons.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class UpdateUserPacketListener implements PacketListener<UpdateUserPacket> {

    private final Callback<Pair<UUID, User>> callback;

    private final ListeningExecutorService executorService;

    private final UserHandler userHandler;

    @Override
    public void onReceive(UpdateUserPacket updateUserPacket) {
        Futures.addCallback(getUserHandler().loadAsynchronously(getExecutorService(), updateUserPacket.getUuid()), new FutureCallbackAdapter<User>() {
            @Override
            public void onSuccess(@Nullable User user) {
                if (getCallback() != null) getCallback().invoke(new Pair<>(updateUserPacket.getUuid(), user));
            }
        }, getExecutorService());
    }
}
