package de.leon.commons.netty;

import com.google.common.util.concurrent.ListeningExecutorService;
import de.leon.commons.netty.handler.ClientChannelHandler;
import de.leon.commons.netty.handler.ServerChannelHandler;
import de.leon.commons.netty.packet.Packet;
import de.leon.commons.netty.packet.PacketListener;
import de.leon.commons.netty.packet.packets.DistributionPacket;
import de.leon.commons.netty.pipeline.Decoder;
import de.leon.commons.netty.pipeline.Encoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class NettyBootstrap {

    @Getter
    public static final Map<Class<? extends Packet>, List<PacketListener<? extends Packet>>> listeners = new HashMap<>();

    public static void setupServerBootstrap(int port) {

        Protocol.init();

        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {

            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class).handler(new LoggingHandler(LogLevel.ERROR)).childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) {
                    preparePipeline(socketChannel, new ServerChannelHandler());
                }
            }).option(ChannelOption.SO_BACKLOG, 128).childOption(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.bind(port).sync().channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static void setupClientBootstrap(String host, int port, ListeningExecutorService executorService, DistributionType[] distributionTypes) {

        Protocol.init();
        ClientBootstrap.startClient(host, port, executorService, new ClientChannelHandler(), distributionTypes);

    }

    public static void registerListener(PacketListener<? extends Packet> listener, Class<? extends Packet> packetClass) {
        getListeners().computeIfAbsent(packetClass, integer -> new CopyOnWriteArrayList<>()).add(listener);
    }

    protected static void preparePipeline(Channel channel, SimpleChannelInboundHandler<Packet> handler) {
        channel.pipeline().addLast(new LengthFieldPrepender(4, true), new Decoder(), new Encoder(), handler);
    }
}
