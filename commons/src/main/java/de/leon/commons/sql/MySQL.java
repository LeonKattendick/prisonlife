package de.leon.commons.sql;

import de.leon.commons.sql.result.Result;
import de.leon.commons.sql.result.Row;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Getter
public class MySQL {

    private Connection connection;

    public MySQL(File dataFolder) {
        try {

            MySQLConfiguration configuration = new MySQLConfiguration(dataFolder);
            configuration.init();

            this.connection = DriverManager.getConnection("jdbc:mysql://" + configuration.getHost() + ":3306/" + configuration.getDatabase() + "?autoReconnect=true", configuration.getUser(), configuration.getPassword());

            System.out.println("[MySQL] Database connection was successfully established.");

        } catch (InvalidConfigurationException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(String query, Object... objects) {

        PreparedStatement statement = null;

        try {

            statement = getConnection().prepareStatement(query);
            for (int i = 0; i < objects.length; i++) statement.setObject(i + 1, objects[i]);

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    public Result query(String query, Object... objects) {

        Result result = new Result();
        PreparedStatement statement = null;

        try {

            statement = getConnection().prepareStatement(query);
            for (int i = 0; i < objects.length; i++) statement.setObject(i + 1, objects[i]);

            ResultSet resultSet = statement.executeQuery();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            List<String> columns = new ArrayList<>();

            for (int i = 1; i < resultSetMetaData.getColumnCount() + 1; i++) columns.add(resultSetMetaData.getColumnName(i));

            while (resultSet.next()) {

                Row row = new Row();
                for (String entry : columns) row.put(entry, resultSet.getObject(entry));
                result.addRow(row);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
        }
        return result;
    }

    public void close() {
        try {
            if (getConnection() != null && !getConnection().isClosed()) getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
