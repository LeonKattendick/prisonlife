package de.leon.commons.sql.result;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Getter
public class Result {

    private final List<Row> rows = new ArrayList<>();

    public void addRow(Row row) {
        getRows().add(row);
    }

    public Iterator<Row> iterator() {
        return getRows().iterator();
    }
}
