package de.leon.commons.sql;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.UUID;

@Getter
@AllArgsConstructor
public abstract class PlayerBasedSQLHandler<T> {

    private final MySQL mySQL;

    private final HashMap<UUID, T> map;

    public T get(UUID uuid) {
        return getMap().get(uuid);
    }

    public void remove(UUID uuid) {
        getMap().remove(uuid);
    }

    public ListenableFuture<T> loadAsynchronously(ListeningExecutorService executorService, UUID uuid) {
        return executorService.submit(() -> load(uuid));
    }

    public ListenableFuture<T> updateAsynchronously(ListeningExecutorService executorService, UUID uuid, T t) {
        return executorService.submit(() -> update(uuid, t));
    }

    public abstract void initTables();

    public abstract boolean isRegistered(UUID uuid);

    public abstract void register(UUID uuid);

    public abstract T load(UUID uuid);

    public abstract T update(UUID uuid, T t);

}
