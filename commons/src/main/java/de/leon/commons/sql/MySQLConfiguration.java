package de.leon.commons.sql;

import lombok.Getter;
import lombok.NonNull;
import net.cubespace.Yamler.Config.YamlConfig;

import java.io.File;

@Getter
public class MySQLConfiguration extends YamlConfig {

    private String host = "localhost", user = "prison", password = "password", database = "prison";

    public MySQLConfiguration(@NonNull File dataFolder) {
        CONFIG_FILE = new File(dataFolder, "mysql.yml");
    }
}
