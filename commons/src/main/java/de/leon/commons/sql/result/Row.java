package de.leon.commons.sql.result;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class Row {

    private final Map<String, Object> content = new HashMap<>();

    public void put(String column, Object content) {
        getContent().put(column, content);
    }

    public String getString(String column) {
        return (String) getContent().get(column);
    }

    public Integer getInt(String column) {
        return (Integer) getContent().get(column);
    }

    public Long getLong(String column) {
        return (Long) getContent().get(column);
    }
}
