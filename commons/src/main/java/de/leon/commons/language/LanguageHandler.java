package de.leon.commons.language;

import lombok.Getter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

@Getter
public class LanguageHandler {

    public static Locale DEFAULT = Locale.US;

    private final Map<String, Language> languageMap = new HashMap<>();

    public void init(File dataFolder) {

        File folder = new File(dataFolder + "/locales/");
        if (!folder.exists()) folder.mkdir();

        for (File entry : folder.listFiles()) {
            if (!entry.getName().endsWith(".properties")) continue;

            Locale locale = Locale.forLanguageTag(entry.getName().replace(".properties", "").replace("_", "-"));
            if (locale == null) continue;

            Properties properties = new Properties();
            InputStreamReader reader = null;

            try {

                reader = new InputStreamReader(new FileInputStream(entry));
                properties.load(reader);

                System.out.println("[LOCALES] Loaded " + locale + " with " + properties.size() + " entries.");

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ignored) {
                    }
                }
            }
            getLanguageMap().put(locale.toString(), new Language(locale, properties));
        }
    }

    public Language get(String id) {
        if (getLanguageMap().containsKey(id)) return getLanguageMap().get(id);
        return getLanguageMap().get(DEFAULT.toString());
    }
}
