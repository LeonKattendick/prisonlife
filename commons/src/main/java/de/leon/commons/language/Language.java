package de.leon.commons.language;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Properties;

@Getter
@RequiredArgsConstructor
public class Language {

    @NonNull
    private final Locale locale;

    @NonNull
    private final Properties properties;

    public String translate(String key, Object... args) {
        if (key == null || !getProperties().containsKey(key)) return "N/A";

        String message = (String) getProperties().get(key);
        String prefix = (String) getProperties().get("prefix");

        if (prefix != null) message = message.replace("%prefix%", prefix);

        MessageFormat format = new MessageFormat(message);
        format.setLocale(getLocale());

        return format.format(args);
    }
}
