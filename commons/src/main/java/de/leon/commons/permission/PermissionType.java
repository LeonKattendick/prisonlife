package de.leon.commons.permission;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PermissionType {

    HEAD_ADMIN("§cHead-Admin", "§c", "001", 100),
    ADMIN("§9Admin", "§9", "003", 90),
    MODERATOR("§3Moderator", "§3", "005", 80),
    SUPPORTER("§3Supporter", "§3", "007", 70),
    PLAYER("§7", "", "009", 0);

    private final String displayName;

    private final String tablist;

    private final String sorting;

    private final int permissionLevel;

    public String getScoreboardTeamName() {
        return getSorting() + toString();
    }

    public static PermissionType getById(String id) {
        for (PermissionType entry : values()) if (entry.toString().equals(id)) return entry;
        return null;
    }
}
