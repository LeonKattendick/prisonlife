package de.leon.commons.permission;

import de.leon.commons.user.User;

public class PermissionHandler {

    public static PermissionType DEFAULT = PermissionType.PLAYER;

    public static boolean hasPermission(User user, PermissionType type) {
        return user.getType() != null && user.getType().getPermissionLevel() >= type.getPermissionLevel();
    }
}
