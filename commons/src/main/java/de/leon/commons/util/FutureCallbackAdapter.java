package de.leon.commons.util;

import com.google.common.util.concurrent.FutureCallback;

public abstract class FutureCallbackAdapter<V> implements FutureCallback<V> {

    @Override
    public void onFailure(Throwable throwable) {
        throwable.printStackTrace();
    }
}
