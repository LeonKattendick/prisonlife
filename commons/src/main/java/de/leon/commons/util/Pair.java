package de.leon.commons.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Pair<A, B> {

    private final A a;

    private final B b;

}
