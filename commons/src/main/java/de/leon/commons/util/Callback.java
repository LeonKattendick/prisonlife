package de.leon.commons.util;

public interface Callback<T> {
    void invoke(T t);
}
