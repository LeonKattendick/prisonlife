package de.leon.commons.user;

import de.leon.commons.language.LanguageHandler;
import de.leon.commons.permission.PermissionHandler;
import de.leon.commons.permission.PermissionType;
import de.leon.commons.sql.MySQL;
import de.leon.commons.sql.PlayerBasedSQLHandler;
import de.leon.commons.sql.result.Result;
import de.leon.commons.sql.result.Row;
import lombok.Getter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;

@Getter
public class UserHandler extends PlayerBasedSQLHandler<User> {

    private final LanguageHandler languageHandler;

    public UserHandler(MySQL mySQL, LanguageHandler languageHandler) {
        super(mySQL, new HashMap<>());
        this.languageHandler = languageHandler;
    }

    @Override
    public void initTables() {
        getMySQL().update("CREATE TABLE IF NOT EXISTS `permissions` (uuid VARCHAR(36) PRIMARY KEY, locale VARCHAR(16), permissionType VARCHAR(64), registeredTime BIGINT)");
    }

    @Override
    public boolean isRegistered(UUID uuid) {

        Result result = getMySQL().query("SELECT * FROM `permissions` WHERE uuid = ? LIMIT 1", uuid.toString());

        return result.iterator().hasNext();
    }

    @Override
    public void register(UUID uuid) {
        getMySQL().update("INSERT INTO `permissions` (uuid, locale, permissionType, registeredTime) VALUES (?, ?, ?, ?)", uuid.toString(), LanguageHandler.DEFAULT.toString(), PermissionHandler.DEFAULT.toString(), System.currentTimeMillis());
    }

    @Override
    public User load(UUID uuid) {

        User user = null;
        Result result = getMySQL().query("SELECT * FROM `permissions` WHERE uuid = ? LIMIT 1", uuid.toString());
        Iterator<Row> iterator = result.iterator();

        if (result.iterator().hasNext()) {
            Row row = iterator.next();
            getMap().put(uuid, user = new User(getLanguageHandler().get(row.getString("locale")), PermissionType.getById(row.getString("permissionType")), row.getLong("registeredTime")));
        }
        return user;
    }

    @Override
    public User update(UUID uuid, User user) {

        getMySQL().update("UPDATE `permissions` SET locale = ?, permissionType = ? WHERE uuid = ?", user.getLanguage().getLocale().toString(), user.getType().toString(), uuid.toString());

        return user;
    }
}
