package de.leon.commons.user;

import de.leon.commons.language.Language;
import de.leon.commons.permission.PermissionType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User {

    private Language language;

    private PermissionType type;

    private long registeredTime;

    public String translate(String key, Object... args) {
        return getLanguage().translate(key, args);
    }

    public boolean isNewUser() {
        return getRegisteredTime() + (1000 * 60 * 60 * 24 * 7) > System.currentTimeMillis();
    }
}
